interface QueueItem<T> {
    shared: T
    index: number
    counter: number
    promise: Promise<QueueItem<T>>
}

/**
 * pWorker creates a work queue based on some shared resource.
 *
 * It guarantees that while the promise returned by `work` is pending, no other
 * call has access to the same shared object.
 *
 * This means concurrency is naturally limited to `shared.length` worker calls
 * at the same time.
 *
 * The returned function waits until there is at least one free shared object,
 * locks it using a Mutex-like mechanism, and then calls its argument.
 * Once the returned promise settles (either resolves or rejects), the
 * shared object is released again, ready to be picked up by other calls.
 *
 * @param shared an array of shared resource objects. every object in the array will be exclusively used by a single call to `work` at the same time.
 */
export default function pWorker<T>(shared: T[]): <R> (work: (shared: T) => Promise<R>) => Promise<R> {
    if (shared.length === 0) {
        throw new Error('cannot create a worker with a concurrency of zero!')
    }

    const queue = shared.map((shared, index) => {
        const state: Partial<QueueItem<T>> = {
            shared,
            index,
            counter: 0
        }

        state.promise = Promise.resolve(state as QueueItem<T>)

        return state as QueueItem<T>
    })

    return async function work<R>(work: (shared: T) => Promise<R>): Promise<R> {
        // this is essentially a compare-exchange loop (see https://en.wikipedia.org/wiki/Compare-and-swap)
        //
        // Promise.race resolves with the first finished QueueItem
        //
        // But we might have multiple Promise.race calls in flight at the same time!
        // this means that multiple fibers can get the same `freeQueueItem` object
        // we want to override the promise down below such that we don't create an
        // infinite promise chain, but that means we either have to
        //  - await freeQueueItem.promise, just moving the goalpost
        //  - not doing that, which means multiple workers could now use the same shared resource
        //
        // do avoid these problems, we add an "atomic" counter to each queueitem,
        // which we compare before accepting a freeQueueItem. Once we accept,
        // we increment the counter on the same tick, which means other
        // in-flight work() calls will loop again.
        let counters
        let freeQueueItem: QueueItem<T>

        do {
            counters = queue.map(q => q.counter)
            freeQueueItem = await Promise.race(queue.map(q => q.promise))
        } while (freeQueueItem.counter !== counters[freeQueueItem.index])

        // we got an item, be careful to stay in the same async tick now.
        freeQueueItem.counter += 1
        if (freeQueueItem.counter === Number.MAX_SAFE_INTEGER) {
            freeQueueItem.counter = 0
        }

        // NO await here!!
        const resultP = work(freeQueueItem.shared)

        freeQueueItem.promise = resultP.then(() => freeQueueItem, () => freeQueueItem)

        // now we can
        const result = await resultP
        return result
    }
}
