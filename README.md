# p-worker

> a single-function package that constructs a function gaining exclusive access to some shared resource

Think of [p-limit](https://www.npmjs.com/package/p-limit), but hard mode.

## Usage

```ts
import pWorker from '@jreusch/p-worker'

// some resource requiring exclusive access -
// browser here for example is a puppeteer instance
const tabs = await Promise.all(Array.from({ length: 4 }, () =>
    browser.newPage()))

// make a worker function
const useTab = pWorker(tabs)

// every call to useTab will have a unique lock on a tab -
// so if it loads a page, no other call to useTab accidentily
// overrides it!
useTab(async tab => /* ... */)
useTab(async tab => /* ... */)
useTab(async tab => /* ... */)
useTab(async tab => /* ... */)
useTab(async tab => /* ... */)

// the returned promise will block until a tab is available,
// lock it until your callback returns, and then resolve
// with the result of your callback
const pdf = await useTab(async tab => {
    await tab.goto('https://joshi.monster')
    const pdf = await tab.pdf()
    return pdf
})
```
